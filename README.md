# Advent of Code 2017

* Language: Kotlin

Puzzle | Difficulty (1 to 4)
-------|-----------
1  | oo
2  | o
3  | oooo
4  | oo
5  | o
6  | oo
7  | ooo
8  | o
9  | oo
10 | oooo
11 | ooo
12 | ooo
13 | oo (math-heavy)
14 | oooo
15 | ooo
16 | oo
17 | oo
18 | o (1st part) oooo (2nd part)
19 | oo !
20 | ooo (math-heavy)
21 | oooo
22 | oooo
23 | o (1st part)
24 | oo
25 | ooo