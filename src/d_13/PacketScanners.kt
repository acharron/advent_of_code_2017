package d_13

import java.io.File

fun main(args: Array<String>) {
    val rows = File("src/d_13/input.txt").readLines()

    // Create layers
    val layers = ArrayList<Int>()
    for (i in 0 until 100) {
        layers.add(i, 0)
    }
    // Init a depth for each layer that has one
    for (row in rows) {
        val params = row.split(" ")
        val layerNumber = params[0].removeSuffix(":").toInt()
        val depth = params[1].toInt()

        layers[layerNumber] = depth
    }

    println("Layers initialized: $layers")


    //// Part 1 ////
    val detectedIndexes = ArrayList<Int>()

    for (layerNumber in 0 until 100) {
        // Skip layers with no depth : can't be detected
        if (layers[layerNumber] == 0) continue

        if ((layerNumber) % (2 * (layers[layerNumber] - 1)) == 0) {
            detectedIndexes.add(layerNumber)
        }
    }

    var result = 0
    for (detected in detectedIndexes) {
        result += detected * layers[detected]
    }
    println("Part 1 : $result")


    //// Part 2 ////
    var delay = 0
    var isDetected = true
    val maxDelay = 10000000

    while (isDetected && delay < maxDelay) {
        isDetected = false

        for (layerNumber in 0 until 100) {
            if (layers[layerNumber] == 0) continue

            if ((layerNumber + delay) % (2 * (layers[layerNumber] - 1)) == 0) {
                isDetected = true
                break
            }
        }
        
        if (!isDetected) println("Part 2 : Hasn't been detected with a delay of $delay !")

        delay++
    }
}

// Old (and wrong) resolution tactic...
private data class Guard(var position: Int, var movingUp: Boolean)

private fun initPositionsGuards(positionsGuard0s: ArrayList<Guard>) {
    for (guard in positionsGuard0s) {
        if (guard.position != 0) guard.position = 1
        guard.movingUp = true
    }
}

private fun updateGuards(positionsGuard0s: ArrayList<Guard>, layers: ArrayList<Int>) {
    for (layerNumber in 0 until 100) {
        val guard = positionsGuard0s[layerNumber]
        val depth = layers[layerNumber]

        // Skip "non existent" guards
        if (guard.position == 0) continue

        if (guard.movingUp) {
            if (guard.position < depth) guard.position++
            else {
                guard.position--
                guard.movingUp = false
            }
        } else {
            if (guard.position > 1) guard.position--
            else {
                guard.position++
                guard.movingUp = true
            }
        }

    }
}
