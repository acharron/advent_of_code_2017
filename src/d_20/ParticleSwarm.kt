package d_20

import java.io.File
import java.lang.Math.abs


private class Particle(val id: Int,
                       var pX: Int, var pY: Int, var pZ: Int,
                       var vX: Int, var vY: Int, var vZ: Int,
                       val aX: Int, val aY: Int, val aZ: Int) {

    var isDead = false

    fun move() {
        vX += aX
        vY += aY
        vZ += aZ

        pX += vX
        pY += vY
        pZ += vZ
    }

    fun totalA(): Int {
        return abs(aX) + abs(aY) + abs(aZ)
    }

    fun getCoordinates() = "$pX,$pY,$pZ"

    override fun toString() = "$id : p=$pX,$pY,$pZ v=$vX,$vY,$vZ a=$aX,$aY,$aZ"
}

private val allParticles = hashSetOf<Particle>()

fun main(args: Array<String>) {
    val rows = File("src/d_20/input.txt").readLines()

    for (i in 0 until rows.size) {
        val params = rows[i].split("<", ">")
        val p = params[1].split(",").map { it.toInt() }
        val v = params[3].split(",").map { it.toInt() }
        val a = params[5].split(",").map { it.toInt() }

        val particle = Particle(i, p[0], p[1], p[2],
                v[0], v[1], v[2],
                a[0], a[1], a[2])
        allParticles.add(particle)
    }

    var min = allParticles.first()
    for (part in allParticles) {
        if (part.totalA() < min.totalA()) min = part
    }
    println("Part 1 : Closest particle overall  $min")

    for (i in 1..1000) {
        val listPositions = hashMapOf<String, Int>()
        for (particle in allParticles.filter { !it.isDead }) {
            particle.move()

            if (listPositions.contains(particle.getCoordinates())) {
                val otherPart = allParticles.first { it.id == listPositions[particle.getCoordinates()] }
                otherPart.isDead = true
                particle.isDead = true
                println("Particle dead! ${particle.id}   (i = $i)")
            } else {
                listPositions[particle.getCoordinates()] = particle.id
            }
        }
    }

    println("Part 2 : Remaining particles after collisions " + allParticles.filter { !it.isDead }.count())
}