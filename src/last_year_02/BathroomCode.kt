package last_year_02

import java.io.File

fun main(args: Array<String>) {
    val rows = File("src/last_year_02/input.txt").readLines()

    var result = ""
    var currentPos = 5

    for (row in rows) {
        for (c in row.toCharArray()) {
            when (c.toString()) {
                "U" -> currentPos = when (currentPos) {
                    1 -> 1
                    2 -> 2
                    3 -> 3
                    4 -> 1
                    5 -> 2
                    6 -> 3
                    7 -> 4
                    8 -> 5
                    9 -> 6
                    else -> 0
                }
                "D" -> currentPos = when (currentPos) {
                    1 -> 4
                    2 -> 5
                    3 -> 6
                    4 -> 7
                    5 -> 8
                    6 -> 9
                    7 -> 7
                    8 -> 8
                    9 -> 9
                    else -> 0
                }
                "L" -> currentPos = when (currentPos) {
                    1 -> 1
                    2 -> 1
                    3 -> 2
                    4 -> 4
                    5 -> 4
                    6 -> 5
                    7 -> 7
                    8 -> 7
                    9 -> 8
                    else -> 0
                }
                "R" -> currentPos = when (currentPos) {
                    1 -> 2
                    2 -> 3
                    3 -> 3
                    4 -> 5
                    5 -> 6
                    6 -> 6
                    7 -> 8
                    8 -> 9
                    9 -> 9
                    else -> 0
                }
            }

            println("${c.toString()} Moved to $currentPos")
        }

        result += currentPos
    }

    println(result)
}