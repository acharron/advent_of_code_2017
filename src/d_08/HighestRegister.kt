package d_08

import java.io.File

private val listRegisters = HashMap<String, Int>()

fun main(args: Array<String>) {
    val rows = File("src/d_08/input.txt").readLines()

    var allTimeMax = 0

    for (row in rows) {
        val params = row.split(" ")

        // Assign parameters
        val target = params[0]
        val instr = params[1]
        val value = params[2].toInt()
        val conditionTarget = params[4]
        val comparator = params[5]
        val conditionValue = params[6].toInt()

        // Create new register if not already present
        if (!listRegisters.contains(target)) listRegisters.put(target, 0)
        if (!listRegisters.contains(conditionTarget)) listRegisters.put(conditionTarget, 0)

        // Check condition
        var conditionOk = false
        val a = listRegisters[conditionTarget]
        if (a != null) {
            conditionOk = when(comparator) {
                ">"     -> a > conditionValue
                ">="    -> a >= conditionValue
                "<"     -> a < conditionValue
                "<="    -> a <= conditionValue
                "=="    -> a == conditionValue
                "!="    -> a != conditionValue
                else    -> false
            }
        }

        // Update target register value
        if (conditionOk) {
            val oldValue = listRegisters[target]
            var newValue = 0
            if (oldValue != null) {
                when (instr) {
                    "inc" -> newValue = oldValue + value
                    "dec" -> newValue = oldValue - value
                }
            }

            listRegisters.replace(target, newValue)
            // Part 2 : Update all time max
            if (newValue > allTimeMax) allTimeMax = newValue
        }
    }

    // Part 1 : Find highest final value
    var maxValue = 0
    var maxRegister = ""
    for (reg in listRegisters) {
        if (reg.value > maxValue) {
            maxValue = reg.value
            maxRegister = reg.key
        }
    }

    println("Part 1 : Max register $maxRegister $maxValue")

    println("Part 2 : All time max $allTimeMax")
}