package d_14

import d_10.getKnotHash

private val input = "hfdlxzhv"
// Input example
//private val input = "flqrgnkx"

//Part 2
private val matrice = ArrayList<MutableList<Int>>()
private var nbGroups = 0

fun main(args: Array<String>) {
    val gridSize = 128

    // Part 1 : Total count of cells set to 1
    var totalCount = 0

    for (i in 0 until gridSize) {
        val key = input + "-$i"
        val knotHash = getKnotHash(key.toByteArray())

        var row = ""
        // Parse the knot hash to hex value of 4 bits
        for (s in knotHash.toCharArray().map { it.toString() }) {
            row += Integer.toBinaryString(Integer.parseInt(s, 16)).padStart(4, '0')
        }
        println(row)

        val rowInt = row.toCharArray().map { it.toString().toInt() }.toMutableList()
        rowInt.forEach { if (it == 1) totalCount++ }
        matrice.add(rowInt)
    }

    println("Part 1 : $totalCount")

    // If there's a 1, it means it's not in a group yet.
    // Values registered in a group are set to 2
    while (matrice.any { it.any { it == 1 } }) {
        val nextRow = matrice.indexOfFirst{ it.contains(1) }
        val nextCol = matrice[nextRow].indexOfFirst { it == 1 }

        nbGroups++
        // Recursive call : Set the value to 2, and update all the neighbours
        updateGroup(nextRow, nextCol)
    }

    println("Part 2 : nbGroups = $nbGroups")
}

private fun updateGroup(row: Int, col: Int) {
    matrice[row][col] = 2

    if (row - 1 >= 0 && matrice[row - 1][col] == 1) updateGroup(row - 1, col)
    if (row + 1 < matrice.size && matrice[row + 1][col] == 1) updateGroup(row + 1, col)
    if (col - 1  >= 0 && matrice[row][col - 1] == 1) updateGroup(row, col - 1)
    if (col + 1 < matrice[row].size && matrice[row][col + 1] == 1) updateGroup(row, col + 1)
}