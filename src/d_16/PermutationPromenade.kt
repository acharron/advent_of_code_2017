package d_16

import java.io.File

private var programs = "a b c d e f g h i j k l m n o p".split(" ").toMutableList()
//private var programs = "a b c d e".split(" ").toMutableList()


fun main(args: Array<String>) {
    val rows = File("src/d_16/input.txt").readLines()
    val params = rows[0].split(",")

    var cycle = 0

    for (i in 1..1000) {
        for (param in params) {
            val instr = param[0].toString()
            val rest = param.removeRange(0..0)

            when (instr) {
                "s" -> spin(rest.toInt())
                "x" -> {
                    val indexes = rest.split("/").map { it.toInt() }
                    exchange(indexes[0], indexes[1])
                }
                "p" -> {
                    val p = rest.split("/")
                    partner(p[0], p[1])
                }
            }
        }
        println("$i \t ${programs.joinToString("")}")
        // On cherche la période cyclique
        if ("abcdefghijklmnop" == programs.joinToString("")) {
            println("Trouvé !")
            cycle = i
            break
        }
    }

    val k = 1000000000
    println("$k $cycle ${k % cycle}")
    println("La réponse à la partie 2 est donc à l'indice ${k % cycle}")
}

private fun spin(length: Int) {
    val subEnd = programs.subList(programs.size - length, programs.size)
    val subStart = programs.subList(0, programs.size - length)
    val newList = ArrayList<String>()
    newList.addAll(subEnd)
    newList.addAll(subStart)
    programs = newList.toMutableList()
}

private fun exchange(i: Int, j: Int) {
    val buff = programs[i]
    programs[i] = programs[j]
    programs[j] = buff
}

private fun partner(a: String, b: String) {
    exchange(programs.indexOf(a), programs.indexOf(b))
}