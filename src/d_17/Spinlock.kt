package d_17

// Example
//private val input = 3
private val input = 369


fun main(args: Array<String>) {
    var buffer = arrayListOf(0, 1)

    var currentIndex = 1

    for (i in 2..2017) {
        // Moving forward (circular)
        currentIndex += input
        while (currentIndex >= buffer.size) currentIndex -= buffer.size

        // Insert the new value
        val subEnd = buffer.slice((currentIndex + 1)..(buffer.size - 1))
        val subStart = buffer.slice(0..currentIndex)

        val newList = ArrayList<Int>()
        newList.addAll(subStart)
        newList.add(i)
        newList.addAll(subEnd)
        buffer = newList

        // Update currentIndex
        currentIndex++
    }

    println("Part 1: ${buffer[currentIndex - 1]} ${buffer[currentIndex]} [${buffer[currentIndex + 1]}]")


    // Part 2
    var size = 2
    currentIndex = 1
    var currentValue1 = 1
    for (i in 2..50000000) {
        // Moving forward
        currentIndex += input
        while (currentIndex >= size) currentIndex -= size

        // Gardé pour la postérité
        // On regarde sur le prochain round va tomber sur un 0...
        //if ((currentIndex + input) % size == 0) currentValue1 = i + 1
        if (currentIndex == 0) currentValue1 = i

        currentIndex++
        size++
    }

    println("Part 2: $currentValue1")
}