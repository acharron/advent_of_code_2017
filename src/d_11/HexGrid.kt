package d_11

import java.io.File

private enum class Dir {
    N, S, NE, NW, SE, SW
}

private class Relation(val dir: Dir,
                       val opposite: Dir) {
    val composites = HashMap<Dir, Dir>()
}

private val shortestPath = ArrayList<Dir>()

fun main(args: Array<String>) {
    val rows = File("src/d_11/input.txt").readLines()
    val instructions = rows[0].split(",")

    var furthestDistance = 0

    // Pas moyen d'initiliser la HashMap directement, on fait donc ça en 2 étapes...
    val relN = Relation(Dir.N, Dir.S)
    val relS = Relation(Dir.S, Dir.N)
    val relNE = Relation(Dir.NE, Dir.SW)
    val relNW = Relation(Dir.NW, Dir.SE)
    val relSE = Relation(Dir.SE, Dir.NW)
    val relSW = Relation(Dir.SW, Dir.NE)

    // Par exemple N + SW = NW, N + SE = NE, etc. On stocke ces équations dans composites
    relN.composites.putAll(arrayOf(Pair(Dir.SW, Dir.NW), Pair(Dir.SE, Dir.NE)))
    relS.composites.putAll(arrayOf(Pair(Dir.NW, Dir.SW), Pair(Dir.NE, Dir.SE)))
    relNE.composites.putAll(arrayOf(Pair(Dir.NW, Dir.N), Pair(Dir.S, Dir.SE)))
    relNW.composites.putAll(arrayOf(Pair(Dir.NE, Dir.N), Pair(Dir.S, Dir.SW)))
    relSE.composites.putAll(arrayOf(Pair(Dir.SW, Dir.S), Pair(Dir.N, Dir.NE)))
    relSW.composites.putAll(arrayOf(Pair(Dir.SE, Dir.S), Pair(Dir.N, Dir.NW)))

    for (instr in instructions) {
        val rel = when (instr) {
            "n" -> relN
            "s" -> relS
            "ne" -> relNE
            "nw" -> relNW
            "se" -> relSE
            "sw" -> relSW
            else -> null
        }

        if (rel != null) updateShortestPath(rel)
        else println("ERROR : Invalid input $instr")

        if (shortestPath.size > furthestDistance) furthestDistance = shortestPath.size
    }

    println("Shortest distance ${shortestPath.size}")

    //// Part 2 ////
    println("Furthest distance $furthestDistance")
}

// Si présent, on enlève les opposés, sinon on vérifie les composites, sinon on ajoute la nouvelle direction
private fun updateShortestPath(rel: Relation) {
    if (shortestPath.contains(rel.opposite)) shortestPath.remove(rel.opposite)
    else {
        var updated = false
        for (composite in rel.composites) {
            if (shortestPath.contains(composite.key)) {
                shortestPath.remove(composite.key)
                shortestPath.add(composite.value)
                updated = true
                break
            }
        }
        if (!updated) shortestPath.add(rel.dir)
    }
}