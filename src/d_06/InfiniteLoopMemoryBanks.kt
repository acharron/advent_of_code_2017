package d_06

private val input = "10\t3\t15\t10\t5\t15\t5\t15\t9\t2\t5\t8\t5\t2\t3\t6"

private val statesMap: HashMap<Int, Array<Int>> = HashMap()

fun main(args: Array<String>) {
    var cycles = 0

    val banks: Array<Int> = input.split("\t").map { it.toInt() }.toTypedArray()

    var currentState = banks.clone()

    while (!doesMapContainState(currentState) && cycles < 100000) {

        statesMap.put(cycles, currentState)

        var highestInt = 0
        var currentIndex = 0
        var i = 0
        while (i < banks.size) {
            if (highestInt < banks[i]) {
                highestInt = banks[i]
                currentIndex = i
            }
            i++
        }

        banks[currentIndex] = 0
        while(highestInt > 0) {
            currentIndex++
            if (currentIndex > banks.size - 1) currentIndex = 0

            banks[currentIndex]++
            highestInt--
        }

        currentState = banks.clone()
        cycles++
    }

    println("Number of cycles $cycles")
}

fun doesMapContainState(currentState: Array<Int>): Boolean {
    for (state in statesMap) {
        var i = 0
        var statesIdentical = true

        while (i < state.value.size - 1) {
            if (state.value[i] != currentState[i]) {
                statesIdentical = false
                break
            }
            i++
        }

        if (statesIdentical) {
            println("Identical states! First encountered in ${state.key}")
            return true
        }
    }

    return false
}

