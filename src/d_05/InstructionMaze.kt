package d_05

import java.io.File

fun main(args: Array<String>) {
    val rows: Array<Int> = File("src/d_05/input.txt").readLines().map { it.toInt() }.toTypedArray()

    var steps = 0
    var currentIndex = 0

    while (currentIndex >= 0 && currentIndex < rows.size) {
        val buffIndex = currentIndex
        currentIndex += rows[currentIndex]

        if (rows[buffIndex] >= 3) rows[buffIndex]--
        else rows[buffIndex]++

        steps++
    }

    println(steps)
}