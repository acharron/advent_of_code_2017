package d_03

import java.lang.Math.abs

// Puzzle 1
// Première tentative de résolution. Après avoir trouvé des propriétés intéressantes au carré,
// j'en ai déduit la distance. SpiralMemory2 génère la grille et permet de faire les 2 problèmes facilement
fun main(args: Array<String>) {
    val input = 265149
    //val input = 28
    val result:Int

    var n = 1
    while (input > n*n) {
        n += 2
    }

    val stepsSide = n - 1
    val cornerSE = n * n
    val cornerSO = cornerSE - stepsSide
    val cornerNO = cornerSE - stepsSide * 2
    val cornerNE = cornerSE - stepsSide * 3

    val straightS = cornerSE - (n-1)/2
    val straightO = cornerSO - (n-1)/2
    val straightN = cornerNO - (n-1)/2
    val straightE = cornerNE - (n-1)/2

    when (input) {
        cornerSE -> result = n - 1
        cornerSO -> result = n - 1
        cornerNO -> result = n - 1
        cornerNE -> result = n - 1
        in cornerSO..cornerSE -> {
            result = (n - 1)/2 + abs(input - straightS)
        }
        in cornerNO..cornerSO -> {
            result = (n - 1)/2 + abs(input - straightO)
        }
        in cornerNE..cornerNO -> {
            result = (n-1)/2 + abs(input - straightN)
        }
        in ((n-2)*(n-2) + 1)..cornerNE -> {
            result = (n-1)/2 + abs(input - straightE)
        }

        else -> result = -1
    }

    println("n $n  n² $cornerSE  n²-(n-1) $cornerSO  " +
            "n²-(n-1)*2 $cornerNO  n²-(n-1)*3 $cornerNE")

    println("Result $result")
}
