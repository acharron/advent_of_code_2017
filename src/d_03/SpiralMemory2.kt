package d_03

private data class GridPoint(val x: Int, val y:Int, val value:Int)

private class Grid(firstValue: Int) {
    private val data = ArrayList<GridPoint>()
    private var currentX = 0
    private var currentY = 0
    private var maxX = 0

    // Primary constructor
    init {
        data.add(GridPoint(0, 0, firstValue))
    }


    fun last(): GridPoint {
        return data.last()
    }

    fun addNextPoint() {
        goToNextCoordinates()
        // Puzzle 1
        //val nextValue = data.last().value + 1
        // Puzzle 2
        val nextValue = sumNeighbours(currentX, currentY)

        data.add(GridPoint(currentX, currentY, nextValue))
    }

    private fun getOrZero(x: Int, y: Int): Int {
        return data.firstOrNull { it.x == x && it.y == y }?.value ?: 0
    }

    private fun goToNextCoordinates() {
        if (currentX >= 0 && currentY <= 0 && currentX == -currentY) {
            maxX++
            currentX++
        }
        else if (currentX == maxX && currentY < maxX) {
            currentY++
        }
        else if (currentY == maxX && currentX > -maxX) {
            currentX--
        }
        else if (currentX == -maxX && currentY > -maxX) {
            currentY--
        }
        else if (currentY == -maxX && currentX < maxX) {
            currentX++
        }
    }

    private fun sumNeighbours(x: Int, y: Int): Int {
        return getOrZero(x + 1, y) +
                getOrZero(x + 1, y + 1) +
                getOrZero(x, y + 1) +
                getOrZero(x - 1, y + 1) +
                getOrZero(x - 1, y) +
                getOrZero(x - 1, y - 1) +
                getOrZero(x, y - 1) +
                getOrZero(x + 1, y - 1)
    }
}


fun main(args: Array<String>) {
    val input = 265149

    val grid = Grid(1)

    while (grid.last().value < input) {
        grid.addNextPoint()
    }

    println(grid.last())
}