package d_07

import java.io.File

private class Program(private val name: String) {
    val tops = ArrayList<Program>()

    var bottom: Program? = null
    var weight = 0
    var totalWeight = 0
    var isWeightFinal = false

    override fun toString(): String {
        return "$name ($weight)  ($totalWeight) $isWeightFinal"
    }
}

private val listPrograms = HashMap<String, Program>()

fun main(args: Array<String>) {
    val rows = File("src/d_07/input.txt").readLines()

    // Construct tree
    for (row in rows) {
        val parts = row.split(" ")

        val prgName = parts[0]
        val prg: Program?

        // Init program and set weight
        if (listPrograms.contains(prgName)) {
            prg = listPrograms[prgName]
        } else {
            prg = Program(prgName)
            listPrograms.put(prgName, prg)
        }

        prg?.weight = parts[1].removeSuffix(")").removePrefix("(").toInt()

        // Check for tops
        if (parts.size > 2) {
            var i = 3
            while (i < parts.size) {
                val topPrgName = parts[i].removeSuffix(",")

                val topPrg: Program?

                if (listPrograms.contains(topPrgName)) {
                    topPrg = listPrograms[topPrgName]
                    if (topPrg != null) prg?.tops?.add(topPrg)
                } else {
                    topPrg = Program(topPrgName)
                    prg?.tops?.add(topPrg)
                    listPrograms.put(topPrgName, topPrg)
                }

                topPrg?.bottom = prg

                i++
            }
        }
    }

    //// Part 1 ////

    // Bottom of the tree
    println(listPrograms.values.filter { it.bottom == null })


    //// Part 2 ////

    // Init total weight for the leaves
    for (prg in listPrograms.values.filter { it.tops.size == 0 }) {
        prg.bottom?.totalWeight?.plus(prg.weight)
        prg.totalWeight = prg.weight
        prg.isWeightFinal = true
    }

    var cycles = 0
    var culpritFound = false

    while (listPrograms.values.any { !it.isWeightFinal } && !culpritFound && cycles < 100000) {

        // Take all nodes where the leaves all have their total weight calculated
        val nodes = listPrograms.values.filter { !it.isWeightFinal && it.tops.all { t -> t.isWeightFinal } }

        for (node in nodes) {
            // Check for unbalance
            var stageWeight = 0
            for (top in node.tops) {
                if (stageWeight == 0) {
                    stageWeight = top.totalWeight
                } else if (top.totalWeight != stageWeight) {
                    println("Found the culprit!")
                    println(node.tops)
                    culpritFound = true
                }
            }

            // Update node total weight
            node.totalWeight = node.tops.sumBy { it.totalWeight } + node.weight
            node.isWeightFinal = true
        }

        // Just to prevent infinite loops
        cycles++
    }

}