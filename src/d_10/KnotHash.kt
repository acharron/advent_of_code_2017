package d_10

import java.io.File
import java.lang.Integer.toHexString

// Used in part 2
private val additionalLengths = byteArrayOf(17, 31, 73, 47, 23)

private var currentIndex = 0
private var skipSize = 0

fun main(args: Array<String>) {
    val rows = File("src/d_10/input.txt").readLines()
    val listSize = 256

    //// Part 1 ////
    // Init work array
    val workArray = ArrayList<Int>()
    initWorkArray(workArray, listSize)

    val lengths = rows[0].split(",").map { it.toInt() }
    applyKnotHashRound(workArray, lengths)
    println("Part 1 : Product of the first 2 indexes: ${workArray[0] * workArray[1]}")


    //// Part 2 ////
    // Prepare input (read  byte by byte, instead of an int array) + Append arbitrary bytes (see puzzle)
    val knotHash = getKnotHash(rows[0].toByteArray())

    println("Part 2 : Knot Hash: $knotHash")
}

fun initWorkArray(list: ArrayList<Int>, listSize: Int) {
    var i = 0
    while (i < listSize) {
        list.add(i)
        i++
    }
}

fun applyKnotHashRound(list: ArrayList<Int>, lengths: List<Int>) {
    for (length in lengths) {
        val subList = ArrayList<Int>()

        // On prend la sous-liste
        //println("currentIndex $currentIndex length $length size ${list.size}")
        if (currentIndex + length < list.size) {
            subList.addAll(list.subList(currentIndex, currentIndex + length))
        } else {
            subList.addAll(list.subList(currentIndex, list.size))
            subList.addAll(list.subList(0, currentIndex + length - list.size))
        }

        subList.reverse()

        // On remet dans l'array principal
        var indexMain = currentIndex
        var indexSub = 0
        while (indexSub < subList.size) {

            if (indexMain == list.size) indexMain = 0

            list[indexMain] = subList[indexSub]

            indexSub++
            indexMain++
        }

        // Update index et skip size
        currentIndex += length + skipSize
        // Il est possible d'avoir un currentIndex qui fasse 2 fois le tour de la liste : on utilise donc un while
        while (currentIndex >= list.size) {
            currentIndex -= list.size
        }

        skipSize++
    }
}

fun getDenseHashArray(list: ArrayList<Int>): IntArray {
    val result = IntArray(16)
    for (block in 0..15) {
        // Premier indice du bloc courant
        val j = 16*block

        // 1ère tentative, gardée pour la postérité
        // Si c = a ^ b alors a = c ^ b
        // Pour appliquer XOR en boucle, on prend le premier indice, on le xor à lui-même, pour le rechoper dans la boucle
        //var hash = workArray2[j].xor(workArray2[j])
        //for (index in j..(j+15)) {
        //    hash = hash.xor(workArray2[index])
        //}

        // On peut aussi lancer la boucle de j+1, ça marche aussi...
        var hash = list[j]
        for (index in (j+1)..(j+15)) {
            hash = hash.xor(list[index])
        }

        result[block] = hash
    }

    return result
}

fun getKnotHash(input: ByteArray): String {
    val workArray2 = ArrayList<Int>()
    initWorkArray(workArray2, 256)
    currentIndex = 0
    skipSize = 0

    val key = input.plus(additionalLengths)

    // Apply Knot Hash 64 rounds
    for (round in 1..64) {
        applyKnotHashRound(workArray2, key.map { it.toInt() })
    }

    // XOR by blocks of 16 to get the dense hash
    val denseHashArray = getDenseHashArray(workArray2)

    // On map aux valeurs en hexa, en n'oubliant pas de mettre les 0 devant s'il y en a besoin
    val hexDenseHash = denseHashArray.map { it -> toHexString(it).padStart(2, '0') }
    val result = StringBuffer()
    hexDenseHash.joinTo(result, "")

    return result.toString()
}