package d_19

import java.io.File

private enum class Dir {
    H, B, G, D
}

private val rows = File("src/d_19/input.txt").readLines().map { it.toCharArray() }

fun main(args: Array<String>) {
    val encounteredLetters = ArrayList<String>()
    var canContinue = true
    var dir = Dir.B
    var x = 0
    var y = rows[x].indexOfFirst { it == '|' }

    var steps = 0

    while (canContinue) {
        when (dir) {
            Dir.B -> {
                if (x + 1 < rows.size) {
                    x++
                    if (rows[x][y] == '+') {
                        dir = if (!isCellEmpty(x, y-1)) Dir.G else Dir.D
                    } else {
                        if (rows[x][y] != '-' && rows[x][y] != '|') encounteredLetters.add(rows[x][y].toString())
                    }
                }
            }
            Dir.H -> {
                if (x - 1 >= 0) {
                    x--
                    if (rows[x][y] == '+') {
                        dir = if (!isCellEmpty(x, y-1)) Dir.G else Dir.D
                    } else {
                        if (rows[x][y] != '-' && rows[x][y] != '|') encounteredLetters.add(rows[x][y].toString())
                    }
                }
            }
            Dir.D -> {
                if (y + 1 < rows[x].size) {
                    y++
                    if (rows[x][y] == '+') {
                        dir = if (!isCellEmpty(x-1, y)) Dir.H else Dir.B
                    } else {
                        if (rows[x][y] != '-' && rows[x][y] != '|') encounteredLetters.add(rows[x][y].toString())
                    }
                }
            }
            Dir.G -> {
                if (y - 1 >= 0) {
                    y--
                    if (rows[x][y] == '+') {
                        dir = if (!isCellEmpty(x-1, y)) Dir.H else Dir.B
                    } else {
                        if (rows[x][y] != '-' && rows[x][y] != '|') encounteredLetters.add(rows[x][y].toString())
                    }
                }
            }
        }

        steps++

        if (rows[x][y] == ' ') canContinue = false
    }

    println(encounteredLetters.joinToString(""))
    println("Part 2: steps $steps")
}

private fun isCellEmpty(x: Int, y: Int): Boolean {
    if (x < 0 || x > rows.size || y < 0 || y > rows[x].size) return true
    if (rows[x][y] == ' ') return true

    return false
}