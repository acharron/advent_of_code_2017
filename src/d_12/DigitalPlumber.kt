package d_12

import java.io.File

private class Program(val id: Int) {
    val directNeighbours = ArrayList<Program>()

    // Flag indicating if his direct neighbours were added to a group
    var neighboursCheck = false

    override fun toString(): String {
        return "PID $id"
    }
}

private val allPrograms = HashSet<Program>()
private val allGroups = ArrayList<Set<Program>>()

fun main(args: Array<String>) {
    val rows = File("src/d_12/input.txt").readLines()

    // Construct list of all programs, with their neighbours
    for (row in rows) {
        val params = row.split(" ")

        val progId = params[0].toInt()
        val prog = getOrAdd(progId)

        // Init neighbours
        for (i in 2 until params.size) {
            val neighbourId = params[i].removeSuffix(",").toInt()
            val neighbour = getOrAdd(neighbourId)
            if (neighbour != null) prog?.directNeighbours?.add(neighbour)
        }
    }


    // Puzzle logic
    var counter = 0
    while (allPrograms.any { !it.neighboursCheck } && counter < 10000) {
        // Take the first program not checked yet, and create its group
        val prog = allPrograms.first{ !it.neighboursCheck }
        val newGroup = HashSet<Program>()

        allGroups.add(newGroup)

        newGroup.add(prog)
        newGroup.addAll(prog.directNeighbours)
        prog.neighboursCheck = true

        while (newGroup.any { !it.neighboursCheck }) {
            val next = newGroup.first { !it.neighboursCheck }

            newGroup.addAll(next.directNeighbours)
            next.neighboursCheck = true
        }

        counter++
    }
    if (counter == 10000) println("Exit infinite loop!")

    val group0 = allGroups.first { it.any { it.id == 0 } }
    println("Part 1 : Group with 0 : Size=${group0.size}  $group0")
    println("Part 2 : All groups ${allGroups.size}")
}

private fun getOrAdd(progId: Int): Program? {
    val prog: Program?

    if (allPrograms.any { it.id == progId }) prog = allPrograms.find { it.id == progId }
    else {
        prog = Program(progId)
        allPrograms.add(prog)
    }

    return prog
}