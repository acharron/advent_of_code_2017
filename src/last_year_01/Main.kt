package last_year_01

import java.io.File
import java.lang.Math.abs

private enum class Direction {
    NORD, EST, SUD, OUEST
}

private var currentDir = Direction.NORD

private val visitedCoordinates = ArrayList<Pair<Int, Int>>()

fun main(args: Array<String>) {
    visitedCoordinates.add(Pair(0, 0))

    val instructions = File("src/d_05/input.txt").readLines()
            .map { it.removeSuffix(", ") }

    for (instr in instructions) {
        val steps = instr.removeRange(0,1).toInt()

        when (instr.substring(0,1)) {
            "R" -> turnRight(steps)
            "L" -> turnLeft(steps)
        }

        val currentPos = visitedCoordinates.last()

        println("Dir: $currentDir \t$steps steps " +
                "\tCurrent pos $currentPos" +
                "\tTotal distance ${abs(currentPos.first) + abs(currentPos.second)}")
    }
}

fun turnLeft(steps: Int) {
    currentDir = when (currentDir) {
        Direction.NORD -> Direction.OUEST
        Direction.OUEST -> Direction.SUD
        Direction.SUD -> Direction.EST
        Direction.EST -> Direction.NORD
    }

    updateDistance(steps)
}

fun turnRight(steps: Int) {
    currentDir = when (currentDir) {
        Direction.NORD -> Direction.EST
        Direction.OUEST -> Direction.NORD
        Direction.SUD -> Direction.OUEST
        Direction.EST -> Direction.SUD
    }

    updateDistance(steps)
}

fun updateDistance(steps: Int) {
    val currentPos = visitedCoordinates.last()

    when (currentDir) {
        Direction.NORD -> {
            for (i in 1..steps) checkDuplicatePosAndAdd(Pair(currentPos.first, currentPos.second + i))
        }
        Direction.SUD -> {
            for (i in 1..steps) checkDuplicatePosAndAdd(Pair(currentPos.first, currentPos.second - i))
        }
        Direction.EST -> {
            for (i in 1..steps) checkDuplicatePosAndAdd(Pair(currentPos.first + i, currentPos.second))
        }
        Direction.OUEST -> {
            for (i in 1..steps) checkDuplicatePosAndAdd(Pair(currentPos.first - i, currentPos.second))
        }
    }
}

fun checkDuplicatePosAndAdd(nextPos: Pair<Int, Int>) {
    if (visitedCoordinates.contains(nextPos)) println("Found! $nextPos Distance ${abs(nextPos.first) + abs(nextPos.second)}")
    visitedCoordinates.add(nextPos)
}

