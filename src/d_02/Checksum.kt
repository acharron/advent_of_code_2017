package d_02

import java.io.File

// Input utilisé dans l'exemple 2
private val inputEx2 = "5\t9\t2\t8\n9\t4\t7\t3\n3\t8\t6\t5"

fun main(args: Array<String>) {
    var checksum = 0
    var checksum2 = 0

    val rows = File("src/d_02/input.txt").readLines()

    for (row in rows) {
        val listInt: List<Int> = row.split("\t").map { x -> x.toInt() }

        // Problème 1
        val diffMaxMin = listInt.max() as Int - listInt.min() as Int
        checksum += diffMaxMin

        // Problème 2
        var i = 0
        while (i < listInt.count()) {
            for (diviseur in listInt.filter { it != listInt[i] }) {
                if (listInt[i] % diviseur == 0) {
                    println("Num ${listInt[i]} \tDiviseur $diviseur \tAdd ${listInt[i] / diviseur}")
                    checksum2 += listInt[i] / diviseur
                    break
                }
            }
            i++
        }
    }

    println("Sum diff max-min: $checksum")
    println("Sum even dividers: $checksum2")
}