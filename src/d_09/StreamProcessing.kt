package d_09

import java.io.File

fun main(args: Array<String>) {
    val rows = File("src/d_09/input.txt").readLines()
    val input = rows[0].toCharArray()

    var totalScore = 0
    var currentGroupValue = 0
    var inGarbage = false

    var totalGarbageCharacters = 0

    var i = 0
    while (i < input.size) {
        val s = input[i].toString()

        when (s) {
            "{" -> {
                if (!inGarbage) currentGroupValue++
                else totalGarbageCharacters++
            }
            "}" -> {
                if (!inGarbage) {
                    totalScore += currentGroupValue
                    currentGroupValue--
                } else {
                    totalGarbageCharacters++
                }
            }
            "!" -> i++  //On ignore le caractère suivant
            "<" -> {
                if (!inGarbage) inGarbage = true
                else totalGarbageCharacters++
            }
            ">" -> inGarbage = false

            else -> if (inGarbage) totalGarbageCharacters++
        }

        i++
    }

    println(totalScore)
    println(totalGarbageCharacters)
}