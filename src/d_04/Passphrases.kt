package d_04

import java.io.File

fun main(args: Array<String>) {
    val rows = File("src/d_04/input.txt").readLines()
    var counter = 0

    for (row in rows) {
        if (isValidPassphrase(row.split(" "))) counter++
    }

    println(counter)
}

fun isValidPassphrase(list: List<String>): Boolean {
    val result: Boolean

    // Puzzle 1
    // Si elles ont la même taille, tous les mots sont uniques
    //result = list.distinct().size == list.size

    // Puzzle 2
    result = !containsAnagram(list)

    return result
}

fun containsAnagram(list: List<String>): Boolean {
    var i = 0

    while (i < list.size) {
        val word1 = list[i]
        for (word2 in list.subList(i + 1, list.size)) {
            if (areAnagrams(word1, word2)) return true
        }
        i++
    }
    return false
}

fun areAnagrams(word1: String, word2: String): Boolean {
    val letterAppearanceMap = hashMapOf<Char, Int>()

    for (c in word1.toLowerCase().toCharArray()) {
        if (letterAppearanceMap.containsKey(c)) letterAppearanceMap[c] = letterAppearanceMap.getValue(c) + 1
        else letterAppearanceMap.put(c, 1)
    }

    for (c in word2.toLowerCase().toCharArray()) {
        if (letterAppearanceMap.containsKey(c)) letterAppearanceMap[c] = letterAppearanceMap.getValue(c) - 1
        else letterAppearanceMap.put(c, -1)
    }

    // If they have the same number of characters, and the same appearances, then they're anagrams
    return letterAppearanceMap.all { it.value == 0 }
}
