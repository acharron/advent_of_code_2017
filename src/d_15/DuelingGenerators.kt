package d_15

private val factorA: Long = 16807
private val factorB: Long = 48271
private val divider: Long = 2147483647

// Part 1
//private val nbIterations = 40000000
// Part 2
private val nbIterations = 5000000
private var totalCount = 0

private val startA: Long = 873
private val startB: Long = 583
// Exemples
//private val startA: Long = 65
//private val startB: Long = 8921

fun main(args: Array<String>) {

    var previousA: Long = startA
    var previousB: Long = startB

    for (i in 1..40000000) {
        val nextA = (previousA * factorA) % divider
        val nextB = (previousB * factorB) % divider
        previousA = nextA
        previousB = nextB

        judge(nextA, nextB)
    }
    println("Part 1 $totalCount")


    val valuesA = ArrayList<Long>()
    val valuesB = ArrayList<Long>()
    previousA = startA
    previousB = startB

    while (valuesA.size < 5000000 || valuesB.size < 5000000) {
        val nextA = (previousA * factorA) % divider
        val nextB = (previousB * factorB) % divider
        previousA = nextA
        previousB = nextB

        if (nextA % 4 == 0L) valuesA.add(nextA)
        if (nextB % 8 == 0L) valuesB.add(nextB)
    }

    totalCount = 0
    for (i in 0 until 5000000) {
        judge(valuesA[i], valuesB[i])
    }
    println("Part 2 $totalCount")
}

private fun judge(valueA: Long, valueB: Long) {
    val byteA = valueA.and(0xffff)
    val byteB = valueB.and(0xffff)

    if (byteA == byteB) totalCount++
}