package d_18

import java.io.File

private val registers = HashMap<String, Long>()
private var lastPlayedSound = -1L

fun main(args: Array<String>) {
    val rows = File("src/d_18/input.txt").readLines()

    var i = 0
    mainLoop@ while (i < rows.size) {
        val params = rows[i].split(" ")

        if (!registers.containsKey(params[1])) registers.put(params[1], 0)

        when (params[0]) {
            "snd" -> playSound(params[1])
            "set" -> set(params[1], params[2])
            "add" -> add(params[1], params[2])
            "mul" -> mul(params[1], params[2])
            "mod" -> modulo(params[1], params[2])
            "rcv" -> {
                val x = if (params[1].toIntOrNull() != null) params[1].toLong() else registers.getValue(params[1])
                if (x != 0L) {
                    println("Recovering $lastPlayedSound")
                    break@mainLoop
                }
            }
            "jgz" -> {
                val x = if (params[1].toIntOrNull() != null) params[1].toLong() else registers.getValue(params[1])
                val y = if (params[2].toIntOrNull() != null) params[2].toLong() else registers.getValue(params[2])

                if (x != 0L) {
                    i += y.toInt()
                    continue@mainLoop
                }
            }
        }

        i++
    }
}

private fun add(x: String, y: String) {
    val value = if (y.toIntOrNull() != null) y.toLong() else registers.getValue(y)

    registers.replace(x, registers.getValue(x) + value)
}

private fun mul(x: String, y: String) {
    val value = if (y.toIntOrNull() != null) y.toLong() else registers.getValue(y)

    registers.replace(x, registers.getValue(x) * value)
}

private fun modulo(x: String, y: String) {
    val value = if (y.toIntOrNull() != null) y.toLong() else registers.getValue(y)

    registers.replace(x, registers.getValue(x) % value)
}

private fun playSound(s: String) {
    lastPlayedSound = registers.getValue(s)
}

private fun set(r: String, y: String) {
    val value = if (y.toIntOrNull() != null) y.toLong() else registers.getValue(y)

    registers.replace(r, value)
}